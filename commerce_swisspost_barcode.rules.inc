<?php

/**
 * Implement hook_rules_action_info().
 */
function commerce_swisspost_barcode_rules_action_info() {
  return array(
    'commerce_swisspost_barcode_generate_action' => array(
      'label' => t('Generate Swiss Post barcode shipping label'),
      'group' => t('Swiss Post'),
      'named parameter' => TRUE,
      'parameter' => array(
        'sender_name' => array(
          'type' => 'text',
          'label' => t('Sender name'),
        ),
        'sender_city' => array(
          'type' => 'text',
          'label' => t('Sender city'),
        ),
        'sender_street' => array(
          'type' => 'text',
          'label' => t('Sender street'),
        ),
        'sender_zip' => array(
          'type' => 'text',
          'label' => t('Sender postal code'),
        ),
        'recipient_title' => array(
          'type' => 'text',
          'label' => t('Recipient title'),
        ),
        'recipient_first_name' => array(
          'type' => 'text',
          'label' => t('Recipient first name'),
        ),
        'recipient_name' => array(
          'type' => 'text',
          'label' => t('Recipient last name'),
        ),
        'recipient_street' => array(
          'type' => 'text',
          'label' => t('Recipient street'),
        ),
        'recipient_zip' => array(
          'type' => 'text',
          'label' => t('Recipient postal code'),
        ),
        'recipient_city' => array(
          'type' => 'text',
          'label' => t('Recipient city'),
        ),
        'recipient_country' => array(
          'type' => 'text',
          'label' => t('Recipient country'),
          'options list' => '_commerce_swisspost_barcode_country_options_list',
          'restriction' => 'input',
        ),
        'shipping_service' => array(
          'type' => 'text',
          'label' => t('Shipping service'),
          'options list' => '_commerce_swisspost_barcode_service_options_list',
          'restriction' => 'input',
        ),
      ),
      'provides' => array(
        'shipping_label' => array(
          'type' => 'file',
          'label' => t('Shipping label'),
        ),
      ),
    ),
  );
}

function commerce_swisspost_barcode_generate_action($params) {
  $sender = array(
    'name' => $params['sender_name'],
    'city' => $params['sender_city'],
    'street' => $params['sender_street'],
    'zip' => $params['sender_zip']
  );

  $recipient = array(
    'title' => $params['recipient_title'],
    'first_name' => $params['recipient_first_name'],
    'name' => $params['recipient_name'],
    'street' => $params['recipient_street'],
    'zip' => $params['recipient_zip'],
    'city' => $params['recipient_city'],
    'country' => $params['recipient_country'],
  );

  $options = array(
    'service' => $params['shipping_service'],
  );

  if($file = commerce_swisspost_barcode_generate($sender, $recipient, $options)) {
    return array('shipping_label' => $file);
  }
}

function _commerce_swisspost_barcode_country_options_list() {
  return array(
    'CH' => t('Switzerland'),
  );
}

function _commerce_swisspost_barcode_service_options_list() {
  return array(
    'ECO' => t('PostPac Economy'),
    'PRI' => t('PostPac Priority')
  );
}