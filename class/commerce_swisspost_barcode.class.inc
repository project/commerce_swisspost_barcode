<?php

class SwissPostBarcodeRequest {
  public $LabelDefinition = NULL;

  public $FileInfos = array();
  public $Language = 'de';
  public $CustomerSystem = 'PHP Client System';

  public $Sender = NULL;
  public $Recipients = array();

  public $Request = array();

  public $Response = NULL;
  public $LabelFile = NULL;

  function __construct() {
    $this->Request['Envelope']['FileInfos'] = array(
      'FrankingLicense' => variable_get('commerce_swisspost_franking_license', ''),
      'PpFranking' => false,
      'Customer' => array(),
    );
    $this->Request['Envelope']['CustomerSystem'] = $this->CustomerSystem;
    $this->Request['Envelope']['Data']['Provider']['Sending']['Item'] = array();
    $this->Request['Language'] = $this->Language;
  }

  public function setLabelDefinition(SwissPostBarcodeLabelDefinition $LabelDefinition) {
    $this->Request['Envelope']['LabelDefinition'] = $LabelDefinition;
    return $LabelDefinition;
  }

  public function setCustomer(SwissPostBarcodeCustomer $Customer) {
    $this->Request['Envelope']['FileInfos']['Customer'] = $Customer;
    return $Customer;
  }

  public function addRecipient(SwissPostBarcodeRecipient $Recipient) {
    $this->Request['Envelope']['Data']['Provider']['Sending']['Item'][] = array(
      'ItemId' => 12345,
      'Recipient' => $Recipient,
      'Attributes' => array(
        'PRZL' => array('PRI')
      )
    );
    return $Recipient;
  }

  public function generateLabel() {
    try {
      $endpoint_url = 'https://wsbc.post.ch/wsbc/barcode/v2_2';

      // SOAP Config
      $SOAP_wsdl_file_path='https://wsbc.post.ch/wsbc/barcode/v2_2?wsdl';
      $SOAP_config = array(
        'location' => $endpoint_url,
        'login' => variable_get('commerce_swisspost_franking_user', ''),
        'password' => variable_get('commerce_swisspost_franking_password', ''),
      );
      $SOAP_Client = new SoapClient($SOAP_wsdl_file_path, $SOAP_config);
      $response = $SOAP_Client->GenerateLabel($this->Request);
      $this->Response = $response;
      if(isset($response->Envelope->Data->Provider->Sending->Item->IdentCode)) {
        $file_binary = $response->Envelope->Data->Provider->Sending->Item->Label;

        $destination = 'temporary://postlabel.' . $response->Envelope->LabelDefinition->ImageFileType;

        $replace = FILE_EXISTS_REPLACE;
        if($file = file_save_data($file_binary, $destination, $replace)) {
          return $file;
        }
      }
    } catch (SoapFault $fault) {
          echo('Error in SOAP Initialization: '. $fault -> __toString() .'<br/>');
     exit;
    }
  }
}

class SwissPostBarcodeLabelDefinition {
  public $LabelLayout = 'A5';
  public $PrintAddresses = 'RecipientAndCustomer';
  public $ImageFileType = 'PDF';
  public $ImageResolution = 300;
  public $PrintPreview = false;

  public function setLabelLayout($LabelLayout) {
    $this->LabelLayout = $LabelLayout;
    return $LabelLayout;
  }

  public function setPrintAddresses($PrintAddresses) {
    $this->PrintAddresses = $PrintAddresses;
    return $PrintAddresses;
  }

  public function setImageFileType($ImageFileType) {
    $this->ImageFileType = $ImageFileType;
    return $ImageFileType;
  }

  public function setImageResolution($ImageResolution) {
    $this->ImageResolution = $ImageResolution;
    return $ImageResolution;
  }

  public function setPrintPreview($PrintPreview) {
    $this->PrintPreview = $PrintPreview;
    return $PrintPreview;
  }
}

class SwissPostBarcodeCustomer {
  public $Name1 = NULL;
  public $Name2 = NULL;
  public $Street = NULL;
  public $ZIP = NULL;
  public $City = NULL;
  public $POBox = NULL;
  public $Country = NULL;
  public $DomicilePostOffice = NULL;

  public function setName1($Name1) {
    $this->Name1 = $Name1;
    return $Name1;
  }

  public function setName2($Name2) {
    $this->Name2 = $Name2;
    return $Name2;
  }

  public function setStreet($Street) {
    $this->Street = $Street;
    return $Street;
  }

  public function setZIP($ZIP) {
    $this->ZIP = $ZIP;
    return $ZIP;
  }

  public function setCity($City) {
    $this->City = $City;
    return $City;
  }

  public function setPOBox($POBox) {
    $this->POBox = $POBox;
    return $POBox;
  }

  public function setCountry($Country) {
    $this->Country = $Country;
    return $Country;
  }

  public function setDomicilePostOffice($DomicilePostOffice) {
    $this->DomicilePostOffice = $DomicilePostOffice;
    return $DomicilePostOffice;
  }
}

class SwissPostBarcodeRecipient {
  public $Title = NULL;
  public $Vorname = NULL;
  public $Name1 = NULL;
  public $Name2 = NULL;
  public $Street = NULL;
  public $HouseNo = NULL;
  public $FloorNo = NULL;
  public $MailboxNo = NULL;
  public $ZIP = NULL;
  public $City = NULL;
  public $Country = NULL;
  public $Phone = NULL;
  public $Mobile = NULL;
  public $EMail = NULL;

  public function setTitle($Title) {
    $this->Title = $Title;
    return $Title;
  }

  public function setVorname($Vorname) {
    $this->Vorname = $Vorname;
    return $Vorname;
  }

  public function setName1($Name1) {
    $this->Name1 = $Name1;
    return $Name1;
  }

  public function setName2($Name2) {
    $this->Name2 = $Name2;
    return $Name2;
  }

  public function setStreet($Street) {
    $this->Street = $Street;
    return $Street;
  }

  public function setHouseNo($HouseNo) {
    $this->HouseNo = $HouseNo;
    return $HouseNo;
  }

  public function setFloorNo($FloorNo) {
    $this->FloorNo = $FloorNo;
    return $FloorNo;
  }

  public function setMailboxNo($MailboxNo) {
    $this->MailboxNo = $MailboxNo;
    return $MailboxNo;
  }

  public function setZIP($ZIP) {
    $this->ZIP = $ZIP;
    return $ZIP;
  }

  public function setCity($City) {
    $this->City = $City;
    return $City;
  }

  public function setCountry($Country) {
    $this->Country = $Country;
    return $Country;
  }

  public function setPhone($Phone) {
    $this->Phone = $Phone;
    return $Phone;
  }

  public function setMobile($Mobile) {
    $this->Mobile = $Mobile;
    return $Mobile;
  }

  public function setEMail($EMail) {
    $this->EMail = $EMail;
    return $EMail;
  }
}