<?php

function commerce_swisspost_barcode_settings_form() {
  $form['commerce_swisspost_franking_user'] = array(
    '#title' => t('Franking user'),
    '#type' => 'textfield',
    '#default_value' => variable_get('commerce_swisspost_franking_user', ''),
    '#required' => TRUE,
  );

  $form['commerce_swisspost_franking_password'] = array(
    '#title' => t('Franking password'),
    '#type' => 'textfield',
    '#default_value' => variable_get('commerce_swisspost_franking_password', ''),
    '#required' => TRUE,
  );

  $form['commerce_swisspost_franking_license'] = array(
    '#title' => t('Franking license'),
    '#type' => 'textfield',
    '#default_value' => variable_get('commerce_swisspost_franking_license', ''),
    '#required' => TRUE,
  );

  $form = system_settings_form($form);

  return $form;
}